#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    return str(number) * number
    """Return a string corresponding to the line for number"""


def triangle(number: int):
    if not 1<=number<=9:
        raise ValueError
    triangulo = ""
    for i in range(1, number+1):
        triangulo = str(triangulo) +line(i) + '\n'

    return triangulo
    """Return a string corresponding to the triangle for number"""


def main():
    try:
        number: int = sys.argv[1]
        text = triangle(int(number))
        print(text)
    except ValueError:
        print("Please provide a number (between 1 and 9) as argument.")
if __name__ == '__main__':
    main()
